package bank.controller;

import bank.service.CustomerService;
import bank.service.CustomerServiceImplementation;

public class CustomerController {
	CustomerService refCustomerService;

	// getCustomerService() create a reference of CustomerServiceImplementation class and call 2 methods customerLogin() & customerChoice()
	// customerLogin() prompts user for login ID and password
	// customerChoice() prompts user for a input choice of 1) inserting a new user or 2) logout
	public void getCustomerService() {
		refCustomerService = new CustomerServiceImplementation();
		refCustomerService.customerLogin();
		refCustomerService.customerChoice();
	} // end of getCustomerService

} // end of CustomerController class
