package bank.application;

import bank.controller.CustomerController;

public class CustomerApplication {

	public static void main(String[] args) {

		// Create a reference of CustomerController class to call getCustomerService()
		CustomerController refCustomerController = new CustomerController();
		refCustomerController.getCustomerService();
	} // end of main

} // end of CustomerApplication class
