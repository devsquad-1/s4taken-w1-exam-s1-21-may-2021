package bank.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import bank.dao.CustomerDAO;
import bank.dao.CustomerDAOImplementation;
import bank.pojo.Customer;
import utility.DBUtility;

// CustomerServiceImplementation class container two method to prompt Customer Login ID and Password to login and
// prompt user for a choice of creating a new customer ID and Password or to logout.
public class CustomerServiceImplementation implements CustomerService {

	CustomerDAO refCustomerDAO = new CustomerDAOImplementation();;
	Scanner refScanner = new Scanner(System.in);
	Customer refCustomer;
	StringBuilder currentString = new StringBuilder();
	FileWriter refFileWriter = null;
	File refFile = null;
	BufferedReader reader;

	@Override
	public void customerLogin() {
		System.out.print("Enter Login ID: ");
		String customerLoginID = refScanner.next();
		refScanner.nextLine();

		System.out.print("\nEnter Login Password: ");
		String customerPassword = refScanner.next();

		refCustomer = new Customer();
		refCustomer.setCustomerID(customerLoginID);
		refCustomer.setCustomerPassword(customerPassword);

		// calls the CustomerDAOImplementation boolean method of customerAuthentication
		// to check with the Database is inside
		// passing in the reference of the Customer as parameter
		// if refCustomerDAO.customerAuthentication(refCustomer) returns false, prompt
		// user for login again
		if (!refCustomerDAO.customerAuthentication(refCustomer)) {
			System.out.println("\nLogin Failed\n");
			customerLogin();
		}
		;
	} // end of customerLogin

	// prompt user for login ID and password and call a reference of Customer to set
	// the Login ID and password

	@Override
	public void createAFile() {
		refFile = new File("user.txt");
		boolean created = false;
		try {
			while (!created) {
				created = refFile.createNewFile();
				System.out.println(refFile.getName() + " file created at " + refFile.getAbsolutePath() + "\n");
				showOptions();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // end of createAFile

	public void writeToFile() {
		// Step 1: Get user input to write some data to the file
		Scanner refScanner = new Scanner(System.in);
		System.out.print("Write something to the file: ");
		StringBuilder data = new StringBuilder(refScanner.nextLine());
		currentString = data;

		try {

			refFileWriter = new FileWriter(refFile); // Relative path
			// Step 3:
			refFileWriter.write(data.toString());
//			// Step 3:
//			for (int i = 0; i < data.length(); i++) {
//				refFileWriter.write(data.charAt(i));
//			} // End of for loop

		} catch (IOException e) {
			System.out.println("Exception handled while writing to the file...");
		}
		// Step 4: Close the file
		finally {
			try {
				refFileWriter.close(); // To .close() a FileWriter, need to surround with try catch
			} catch (IOException e) {
				System.out.println("Exception handled while closing the file...");
			}
			System.out.println();
		} // End of finally (Good practice to use finally for file handling)

	}

	public void viewFile() {
		System.out.print("Do you want to view the file? ");
		String userAnswer = refScanner.next();
		if (userAnswer.equalsIgnoreCase("Yes")) {
			try {
				Scanner myReader = new Scanner(refFile);
				System.out.println("Content of the File: ");
				while (myReader.hasNextLine()) {
					String data = myReader.nextLine();
					System.out.println(data);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Goodbye World!");
		}
	}

	public void updateFile() {
		System.out.println("Current Content in File:\n" + currentString);
		refScanner.nextLine();
		System.out.print("Do you want to update the file? ");
		String userAnswer = refScanner.nextLine();
		if (userAnswer.equalsIgnoreCase("Yes")) {
			try {
				System.out.print("Enter your update: ");
				String newString = refScanner.nextLine();
				StringBuilder updatedString = currentString.append(" " + newString);
				refFileWriter = new FileWriter(refFile);
				refFileWriter.write(updatedString.toString());
				refFileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Goodbye World!");
		}
	}

	public void showOptions() {
		boolean stillUpdating = true;
		while (stillUpdating) {
			try {
				System.out.println("Option 1: Write to File");
				System.out.println("Option 2: View File");
				System.out.println("Option 3: Update the File");
				System.out.println("Option 4: Exit");
				System.out.print("\nEnter Choice: ");
				refScanner = new Scanner(System.in);
				int choice = refScanner.nextInt();
				switch (choice) {
				case 1:
					writeToFile();
					break;
				case 2:
					viewFile();
					break;
				case 3:
					updateFile();
					break;
				case 4:
					stillUpdating = false;
					customerChoice();
					break;
				default:
					System.out.println("\nOption not found..\n");
				}
			} catch (InputMismatchException e) {
				System.out.println("\nEnter Option 1 - 3\n");
				customerChoice();
			}
		}
	}

	// prompt user for login ID and password and call a reference of Customer to set
	// the Login ID and password
	@Override
	public void insertCustomerRecords() {

		System.out.print("Enter Login ID: ");
		String customerLoginID = refScanner.next();
		refScanner.nextLine();

		System.out.print("\nEnter Login Password: ");
		String customerPassword = refScanner.next();

		refCustomer = new Customer();
		refCustomer.setCustomerID(customerLoginID);
		refCustomer.setCustomerPassword(customerPassword);

		// calls the CustomerDAOImplementation class insertRecord method and pass in the
		// Customer object reference
		// which takes in the user Login ID and password
		refCustomerDAO = new CustomerDAOImplementation();
		refCustomerDAO.insertRecord(refCustomer);

	} // end of userInputInsertRecord

	// Using a swithch case to prompt user for a choice of inserting a new user
	// input or to logout of the application
	@Override
	public void customerChoice() {
		// try-catch user input mismatch if user enter a option other that number 1 or 2
		try {
			System.out.println("Option 1: Insert New Customer Details");
			System.out.println("Option 2: Logout");
			System.out.println("Option 3: Create A File");
			System.out.print("\nEnter Choice: ");
			refScanner = new Scanner(System.in);
			int choice = refScanner.nextInt();
			switch (choice) {
			case 1:
				insertCustomerRecords(); // calls insertCustomerRecords() implemented in line 46
				customerChoice(); // prompt user for a choice again
				break;
			case 2:
				try {
					Connection refConnection = DBUtility.getConnection();
					refConnection.close(); // close db connection
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					System.out.println("\nClosing Connection..");
					System.out.println("\nSuccessfully Logout..");
				}
				System.exit(0); // close program without error
				break;
			case 3:
				createAFile();
				break;
			default:
				System.out.println("\nOption not found..\n");
				customerChoice();
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println("\nEnter Option 1 or 2\n");
			customerChoice();
		}
	} // end of customerChoice
} // end of CustomerServiceImplementation class
