package bank.service;

public interface CustomerService {

	// insert customer record
	void insertCustomerRecords();

	// prompt user for a choice
	void customerChoice();

	// prompt user for login ID and password
	void customerLogin();

	// prompt user to create a file
	void createAFile();

} // end of CustomerService interface
